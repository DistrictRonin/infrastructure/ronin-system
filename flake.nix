{
  description = "District-Ronin infrastructure in Nix";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    nixops-plugged.url = "git+ssh://git@gitlab.com/DistrictRonin/infrastructure/nixops-plugged";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs =
    { self
    , nixpkgs
    , nixops-plugged
    , flake-utils
    } @ inputs:
    let
      inherit (flake-utils.lib) eachDefaultSystem;

    in
    eachDefaultSystem
      (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in
      {
        packages = import ./packages nixpkgs system;

        devShell = pkgs.mkShell {
          nativeBuildInputs = [
            pkgs.git
            pkgs.git-crypt
            pkgs.nixpkgs-fmt
            self.packages.${system}.nixops-production-shim
            nixops-plugged.packages.${system}.nixops-plugged
          ];

          shellHook = ''
            export NIX_PATH="nixpkgs=${nixpkgs.outPath}"
          '';
        };
      }
      ) // {
      nixosModules = import ./modules;

      nixopsConfigurations =
        let
          production = true;
        in
        import ./deployments inputs // {
          default =
            if production
            then self.outputs.nixopsConfigurations.ronin-production
            else self.outputs.nixopsConfigurations.ronin-testing;
        };
    };
}
