{ self, ... } @ inputs:
let
  secrets = import ../secrets/keys.nix;

  apiToken = secrets.hetzner;
  location = "nbg1";

  makeHetzner =
    name: type:
    { ... }:
    {
      deployment.targetEnv = "hetznercloud";
      deployment.hetznerCloud = {
        inherit apiToken location;
        serverName = name;
        serverType = type;
      };

      services.openssh.permitRootLogin = "prohibit-password";

      users.users = {
        root = {
          initialHashedPassword = "";
          openssh.authorizedKeys.keys = [
            # Oscar
            "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDTyuGXJmdZBbax59XdRAODDBheFodh/GL9d/CafMLASjzQkNwpVaEFtPkW2gJUcwkV0cXORjOpfbQCGnWGIb0V4/+ta6RykEQn1PoKGYgf8hgLCONTXRCwXcliEtRmU4UkMaT9LJ0FuG297O3TjIR85NjOU+216/tm2X/nAVJbk9V6fUTnSYj/QJ/0CH/kMrZaG3QutXBi/+fzkUPKrpBVWbuDU0YDUQeeRI1y0xjTO8FF5cuKeEh8ka4dMn2NABfqjuaSIdCnz+H/BnDjwsQMjTNZU5gMEZkRIfSs+yvdvdqoWwOgNqFVd6cDCdkGn9Ov/npIsFcDPjplw7JwYn7l ostylk@ostylk-PC"
            # Nico
            "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEA+7/hQof/k34tdkPJFHyZOGa7oU0xyZTJbegzvTsyTVWRGJK4dvpp9yw1X6D2pEKda9iXj7T5W3AOIMIsmsxbRK/tBuUPnvnVGGuByRbtx1VOLJ2//559HJdfkdGz+T+QjXhniu0bm0krsDxgE4WNA3/zM7oTudxzkaaCitvoILZTv14UNQMKO6PMp9AtOZ+mQAH1PkbbLdvCFa9yytfLFYVhLyOROVeNIgtnqeocFyaGKZIKjFLW4kBTG9kJgrAAGE3y6ZNv6Fo2N02MfeUYYejUzFWaOQNiLBnwjV8+zku6qZ/7rntJpZKwOAJBNHO97pWE8ulUM7hWi5pLKSl53w== rsa-key-20210106"
          ];
        };
      };
    };

  makeNetwork =
    hostname:
    { resources, pkgs, ... }:
    {
      networking.hostName = hostname;
      networking.extraHosts = with pkgs.lib; builtins.foldl'
        (acc: x: "${acc}${x}\n")
        ""
        (forEach (builtins.attrNames hosts) (host: hosts."${host}" + " ${host}"));

      deployment.hetznerCloud.serverNetworks = [{
        network = resources.hetznerCloudNetworks.beacon;
        privateIpAddress = hosts."${hostname}";
      }];

      networking.interfaces.ens3.useDHCP = true;
      networking.interfaces.ens10.useDHCP = true;

      ronin.useSSL = true;
      ronin.hosts = hosts;
      ronin.packages = self.outputs.packages."${pkgs.system}";
    };

  hosts = {
    vault = "10.0.0.128";
    team = "10.0.0.2";
  };
in
{
  resources.hetznerCloudNetworks.beacon = {
    inherit apiToken;
    ipRange = "10.0.0.0/16";
    subnets = [ "10.0.0.0/24" ];
  };

  resources.hetznerCloudVolumes = let
    mkVolume = size: {
      inherit apiToken location size;
      fsType = "ext4";
    };
  in {
    vaultData = mkVolume 20;
    teamData = mkVolume 10;
  };

  vault =
    { config, resources, pkgs, lib, ... }:
    {
      imports = [
        (makeHetzner "nixops-vault" "cx21")
        (makeNetwork "vault")
      ];

      deployment.keys.dns-token.text = secrets.dns-token;
      deployment.keys.dns-zone.text = secrets.dns-zone;
      environment.systemPackages = with config.ronin.packages; [ hetzner-dns ronin-ldap ];

      fileSystems."/srv".hetznerCloud.volume = resources.hetznerCloudVolumes.vaultData;

      ronin.profile.mysql.enable = true;
      ronin.profile.openldap.enable = true;

      # Port 389: OpenLDAP non-encrypted
      networking.firewall.interfaces.ens10.allowedTCPPorts = [ config.services.mysql.port 389 ];
    };

  team =
    { config, lib, nodes, resources, pkgs, ... }:
    {
      imports = [
        (makeHetzner "nixops-team" "cx21")
        (makeNetwork "team")
      ];

      fileSystems."/srv".hetznerCloud.volume = resources.hetznerCloudVolumes.teamData;

      ronin.profile.pterodactyl-panel.enable = true;
      ronin.profile.nextcloud.enable = true;
    };
}
