{ self, nixpkgs, ... } @ inputs:
let
  mkRoninPkgs =
    system: self.outputs.packages."${system}";

  network = {
    description = "District-Ronin infrastructure";
  };

  defaults = {
    imports = builtins.attrValues self.outputs.nixosModules;
  };
in
{
  ronin-production = import ./production.nix inputs // {
    inherit nixpkgs network defaults;
  };

  ronin-testing = {
    inherit nixpkgs network defaults;
    dev =
      { config, pkgs, ... }:
      {
        ronin.services.pterodactyl-panel.enable = true;
        ronin.services.pterodactyl-panel.package = (mkRoninPkgs pkgs.system).pterodactyl-panel;
        ronin.services.pterodactyl-panel.dataDir = "/srv/panel";
        ronin.services.pterodactyl-panel.mysql = {
          host = "192.168.10.191";
          password = "testtest";
        };

        deployment.targetEnv = "libvirtd";
        deployment.libvirtd.memorySize = 1024;
      };

    node1 =
      { config, pkgs, ... }:
      {
        services.mysql.enable = true;
        services.mysql.package = pkgs.mariadb;

        ronin.services.pterodactyl-wings.enable = true;
        ronin.services.pterodactyl-wings.package = (mkRoninPkgs pkgs.system).pterodactyl-wings;
        ronin.services.pterodactyl-wings.panelUrl = "http://192.168.10.239";
        ronin.services.pterodactyl-wings.nodeToken = "aiWaetxvYd7i3ROZs37vqd3LjAw8Hui6KAjZD72iGJip0emh";
        ronin.services.pterodactyl-wings.nodeNo = "1";

        networking.firewall.allowedTCPPorts = [ config.services.mysql.port ];

        deployment.targetEnv = "libvirtd";
        deployment.libvirtd.memorySize = 4096;
      };
  };
}
