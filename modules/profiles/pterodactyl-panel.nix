{ config, lib, pkgs, nodes, ... }:
with lib; let
  secrets = import ../../secrets/keys.nix;
  cfg = config.ronin.profile.pterodactyl-panel;
in
{
  options.ronin.profile.pterodactyl-panel = {
    enable = mkEnableOption "Setup a pterodactyl panel on this machine";
  };

  config = mkIf cfg.enable {
    ronin.services.pterodactyl-panel = {
      enable = true;
      package = config.ronin.packages.pterodactyl-panel;
      dataDir = "/srv/panel";
      url = "admin.district-ronin.de";

      mysql.host = "vault";
      mysql.port = nodes.vault.config.services.mysql.port;
      mysql.database = "pterodactyl";
      mysql.user = "pterodactyl";
      mysql.password = secrets.mysql-pterodactyl;
    };

    # FIXME: there should be a better solution for this problem
    # FIXME: a lot of services want to use nginx in different namespaces :)
    systemd.services.admin-setup = {
      wantedBy = [ "multi-user.target" ];
      before = [ "nginx.service" ];

      script = ''
        mkdir -p /srv
        cp ${config.ronin.packages.adminer} /srv/panel/public/adminer.php
      '';
    };
  };
}
