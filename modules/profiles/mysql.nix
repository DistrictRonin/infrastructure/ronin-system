{ config, lib, pkgs, ... }:
with lib; let
  cfg = config.ronin.profile.mysql;
in
{
  options.ronin.profile.mysql = {
    enable = mkEnableOption "Setup a mysql database";
  };

  config = mkIf cfg.enable {
    services.mysql = {
      enable = true;
      package = pkgs.mysql;
      bind = config.ronin.hosts.vault;
      dataDir = "/srv/mysql";
    };
  };
}
