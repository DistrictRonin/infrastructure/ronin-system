{ config, lib, pkgs, deployment, ... }:
with lib; let
  secrets = import ../../secrets/keys.nix;
  cfg = config.ronin.profile.nextcloud;
in
{
  options.ronin.profile.nextcloud = {
    enable = mkEnableOption "Setup a nextcloud instance";
  };

  config = mkMerge [
    (mkIf cfg.enable {
      deployment.keys.nextcloud-admin = {
        text = secrets.nextcloud-admin;
        user = "nextcloud";
        group = "nextcloud";
        permissions = "0660";
      };

      services.nextcloud = {
        enable = true;
        home = "/srv/nextcloud";
        package = pkgs.nextcloud21;
        hostName = "team.district-ronin.de";

        autoUpdateApps.enable = true;
        autoUpdateApps.startAt = "05:00:00";

        caching.redis = true;

        maxUploadSize = "4G";

        config = {
          adminpassFile = "/run/keys/nextcloud-admin";
          adminuser = "admin";

          dbtype = "pgsql";
          dbuser = "nextcloud";
          dbhost = "/run/postgresql";
          dbname = "nextcloud";
        };

        poolSettings = {
          pm = "dynamic";
          "pm.max_children" = "8";
          "pm.max_requests" = "500";
          "pm.max_spare_servers" = "4";
          "pm.min_spare_servers" = "1";
          "pm.start_servers" = "1";
        };
      };

      services.postgresql = {
        enable = true;
        dataDir = "/srv/postgres";
        enableTCPIP = false;
        ensureDatabases = [ "nextcloud" ];
        ensureUsers = [
          { name = "nextcloud";
            ensurePermissions."DATABASE nextcloud" = "ALL PRIVILEGES";
          }
        ];
      };

      services.redis = {
        enable = true;
        bind = "127.0.0.1";
      };

      services.nginx = {
        recommendedGzipSettings = true;
        recommendedOptimisation = true;
        recommendedProxySettings = true;
        recommendedTlsSettings = true;
      };

      systemd.services."nextcloud-pre-setup" = {
        requires = [ "nextcloud-admin-key.service" ];
        after = [ "nextcloud-admin-key.service" ];
        wantedBy = [ "nextcloud-setup.service" ];

        script = ''
          chmod o+r,o+x /run/keys/
        '';
      };

      systemd.services."nextcloud-setup" = {
        requires = [ "postgresql.service" "redis.service" "nextcloud-pre-setup" ];
        after = [ "postgresql.service" "redis.service" "nextcloud-pre-setup" ];
      };

      virtualisation.oci-containers = {
        backend = "docker";
        containers.docserver = {
          image = "onlyoffice/documentserver:latest";
          ports = [ "4430:443" ];
          volumes = [
            "/srv/onlyoffice/Data:/var/www/onlyoffice/Data"
          ];
        };
      };

      # FIXME: ugly hack to have ssl certificates on document server
      security.acme.certs."team.district-ronin.de".postRun = ''
        mkdir -p /srv/onlyoffice/Data/certs
        cp key.pem /srv/onlyoffice/Data/certs/onlyoffice.key
        cp fullchain.pem /srv/onlyoffice/Data/certs/onlyoffice.crt
      '';
    })
    (mkIf (cfg.enable && config.ronin.useSSL) {
      services.nextcloud = {
        https = true;
        config.overwriteProtocol = "https";
      };

      services.nginx = {
        virtualHosts."team.district-ronin.de" = {
          forceSSL = true;
          enableACME = true;
        };
      };
    })
  ];
}
