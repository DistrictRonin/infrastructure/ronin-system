{ config, lib, pkgs, ... }:
with lib; let
  cfg = config.ronin.profile.openldap;
in
{
  options.ronin.profile.openldap = {
    enable = mkEnableOption "Setup an OpenLDAP instance on this machine";
  };

  config = mkIf cfg.enable {
    services.openldap =
      let
        roninLdif = pkgs.writeText "ronin.ldif" (builtins.readFile ./openldap-schema.ldif);
      in
      {
        enable = true;
        urlList = [ "ldap:///" "ldapi:///" ];

        settings =
          let
            aclTrim = acl:
              concatStrings (splitString "\n" acl);
          in
          {
            attrs.olcLogLevel = [ "stats" ];
            children = {
              "cn=schema".includes = [
                "${pkgs.openldap}/etc/schema/core.ldif"
                "${roninLdif}"
              ];

              "olcDatabase={-1}frontend".attrs = {
                objectClass = [ "olcDatabaseConfig" "olcFrontendConfig" ];
                olcDatabase = "{-1}frontend";
                olcAccess = [
                  (aclTrim ''
                    to *
                      by dn.exact=uidNumber=0+gidNumber=0,cn=peercred,cn=external,cn=auth manage stop
                      by * none stop
                  '')
                ];
              };

              "olcDatabase={0}config".attrs = {
                objectClass = "olcDatabaseConfig";
                olcDatabase = "{0}config";
                olcAccess = [ "to * by * none break" ];
              };

              "olcDatabase={1}mdb".attrs = {
                objectClass = [ "olcDatabaseConfig" "olcMdbConfig" ];
                olcDatabase = "{1}mdb";
                olcDbDirectory = "/srv/ldap";
                olcSuffix = "dc=ronin,dc=de";

                olcAccess = [
                  (aclTrim ''
                    to attrs=userPassword
                      by self =xw stop
                      by anonymous auth stop
                      by * none break
                  '')
                  (aclTrim ''
                    to *
                      by self write stop
                      by dn.regex="ou=user,dc=ronin,dc=de$" read stop
                      by * none break
                  '')
                ];

                olcRootDN = "cn=supervisor,dc=ronin,dc=de";
                olcRootPW = "{SSHA}lbgbBriog7Xeud4LFxxseFx5IINFgGJZ";
              };
            };
          };
      };
  };
}
