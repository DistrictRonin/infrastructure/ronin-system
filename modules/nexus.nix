{ config, pkgs, lib, ... }:
with lib;
{
  options.ronin.services.nexus = {
    enable = mkEnableOption "Enables Nexus";
  };

  config = mkMerge [
    (mkIf config.ronin.services.nexus.enable {
      services.nexus = {
        enable = true;
        package = pkgs.nexus.override { jre_headless = pkgs.jre8_headless; };
        home = "/srv/nexus";
      };

      services.nginx = {
        enable = true;
        recommendedTlsSettings = true;
        recommendedGzipSettings = true;
        recommendedOptimisation = true;
        recommendedProxySettings = true;
        virtualHosts."dev.district-ronin.de" = {
          locations."/" = {
            proxyPass = "http://127.0.0.1:${builtins.toString config.services.nexus.listenPort}";
          };
        };
      };

      networking.firewall.allowedTCPPorts = [ 80 443 ];
    })
    (mkIf (config.ronin.services.nexus.enable && config.ronin.useSSL) {
      services.nginx.virtualHosts."dev.district-ronin.de" = {
        enableACME = true;
        forceSSL = true;
      };
    })
  ];
}
