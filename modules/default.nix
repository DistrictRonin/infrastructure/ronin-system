{
  common = import ./common.nix;
  nexus = import ./nexus.nix;

  pterodactyl-panel = import ./pterodactyl/panel.nix;
  pterodactyl-wings = import ./pterodactyl/wings.nix;

  profile-mysql = import ./profiles/mysql.nix;
  profile-openldap = import ./profiles/openldap.nix;
  profile-pterodactyl-panel = import ./profiles/pterodactyl-panel.nix;
  profile-nextcloud = import ./profiles/nextcloud.nix;
}
