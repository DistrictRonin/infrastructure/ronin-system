{ config, lib, ... }:
with lib; {
  options.ronin = {
    useSSL = mkEnableOption "Use and request ssl certificates";
    packages = mkOption { };
    hosts = mkOption { };
  };

  config = mkMerge [
    (mkIf config.ronin.useSSL {
      security.acme.acceptTerms = true;
      security.acme.email = "ostylk@googlemail.com";
    })
  ];
}
