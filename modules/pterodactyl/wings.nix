{ config, pkgs, lib, ... }:
with lib;
let
  cfg = config.ronin.services.pterodactyl-wings;
in
{
  options.ronin.services.pterodactyl-wings = {
    enable = mkEnableOption "Enable Pterodactyl Wings";
    package = mkOption {
      type = types.package;
    };
    panelUrl = mkOption {
      type = types.str;
    };

    nodeToken = mkOption {
      type = types.str;
    };
    nodeNo = mkOption {
      type = types.str;
    };
  };

  config = mkMerge [
    (mkIf cfg.enable {
      virtualisation.docker.enable = true;

      services.udev.extraRules = ''
        ACTION=="add|change", KERNEL=="sd*[!0-9]|sr*", ATTR{queue/scheduler}="bfq"
      '';

      networking.firewall.allowedTCPPorts = [ 8080 2022 ];

      systemd.services = {
        ptero-wings-setup = {
          after = [ "network-online.target" ];
          path = [ cfg.package ];
          wantedBy = [ "multi-user.target" ];

          script = ''
            mkdir -p /etc/pterodactyl
            cd /etc/pterodactyl

            if test -f ".installed"; then
                exit 0
            fi

            wings configure --panel-url ${cfg.panelUrl} --token ${cfg.nodeToken} --node ${cfg.nodeNo}

            touch .installed
          '';

          serviceConfig.Type = "oneshot";
        };

        ptero-wings = {
          after = [ "ptero-wings-setup.service" "docker.service" ];
          wants = [ "ptero-wings-setup.service" "docker.service" ];
          requires = [ "docker.service" ];
          partOf = [ "docker.service" ];
          path = [ cfg.package pkgs.stdenv pkgs.shadow ];
          wantedBy = [ "multi-user.target" ];

          serviceConfig.WorkingDirectory = "/etc/pterodactyl";
          serviceConfig.PIDFile = "/var/run/wings/daemon.pid";
          serviceConfig.LimitNOFILE = "4096";
          serviceConfig.Restart = "on-failure";

          script = "wings";
        };
      };
    })
  ];
}
