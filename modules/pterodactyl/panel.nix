{ config, pkgs, lib, ... }:
with lib;
let
  cfg = config.ronin.services.pterodactyl-panel;
  phpFpm = config.services.phpfpm.pools.ptero-panel;

  nginxUser = config.services.nginx.user;
  nginxGroup = config.services.nginx.group;

  phpPackage = pkgs.php80.buildEnv {
    extensions = { enabled, all }:
      (with all;
      enabled ++ [ openssl mysqli pdo mbstring tokenizer bcmath xml curl zip ]
      );
  };

  pteroReplace = pkgs.writeScriptBin "pteroReplace" ''
    ${pkgs.gnused}/bin/sed -i "s/^$1=.*$/$1=$2/g" ${cfg.dataDir}/.env
  '';

  pteroCli = pkgs.writeScriptBin "ptero-cli" ''
    #! ${pkgs.runtimeShell}
    cd ${cfg.dataDir}

    /run/wrappers/bin/sudo -u ${nginxUser} ${phpPackage}/bin/php artisan $@
  '';

  pteroUpdateScript = pkgs.writeScriptBin "pteroUpdate" ''
    mkdir -p ${cfg.dataDir}
    cd ${cfg.dataDir}

    cp -R ${cfg.package}/* .
    chown -R ${nginxUser}:${nginxGroup} .
    chmod -R 755 .

    /run/wrappers/bin/sudo -u ${nginxUser} ${pkgs.php80Packages.composer}/bin/composer install --no-dev --optimize-autoloader
  '';

  pteroUpdateFinScript = pkgs.writeScriptBin "pteroUpdateFin" ''
    ${pteroCli}/bin/ptero-cli migrate --seed --force
  '';

in
{
  options.ronin.services.pterodactyl-panel = {
    enable = mkEnableOption "Enabled Pterodactyl Panel";
    package = mkOption {
      type = types.package;
    };
    dataDir = mkOption {
      type = types.path;
      default = "/srv/pterodactyl-panel";
    };
    url = mkOption {
      type = types.str;
      default = "localhost";
    };
    mysql = {
      host = mkOption {
        type = types.str;
        default = "127.0.0.1";
      };
      port = mkOption {
        type = types.int;
        default = config.services.mysql.port;
      };
      database = mkOption {
        type = types.str;
        default = "pterodactyl";
      };
      user = mkOption {
        type = types.str;
        default = "pterodactyl";
      };
      password = mkOption {
        type = types.str;
      };
    };
    redis = {
      host = mkOption {
        type = types.str;
        default = "127.0.0.1";
      };
      port = mkOption {
        type = types.int;
        default = config.services.redis.port;
      };
      password = mkOption {
        type = with types; nullOr str;
        default = null;
      };
    };
  };

  config = mkMerge [
    (mkIf cfg.enable {
      services.redis = {
        enable = true;
      };

      services.cron = {
        enable = true;
        systemCronJobs = [
          "* * * * * ${pteroCli}/bin/ptero-cli schedule:run >> /dev/null 2>&1"
        ];
      };

      services.phpfpm = {
        pools.ptero-panel = {
          user = nginxUser;
          group = nginxGroup;
          inherit phpPackage;
          phpEnv = {
            PATH = "/run/wrappers/bin:/nix/var/nix/profiles/default/bin:/run/current-system/sw/bin:/usr/bin:/bin";
          };
          settings = mapAttrs (name: mkDefault) {
            "listen.owner" = nginxUser;
            "listen.group" = nginxGroup;
            "pm" = "ondemand";
            "pm.max_children" = "2";
          };
        };
      };

      systemd.services =
        let
          scriptConfig =
            let
              redisPassword =
                if cfg.redis.password == null
                then "null"
                else cfg.redis.password;
            in
            ''
              pteroReplace APP_URL http:\\/\\/${cfg.url}
                        
              pteroReplace DB_HOST ${cfg.mysql.host}
              pteroReplace DB_PORT ${builtins.toString cfg.mysql.port}
              pteroReplace DB_DATABASE ${cfg.mysql.database}
              pteroReplace DB_USERNAME ${cfg.mysql.user}
              pteroReplace DB_PASSWORD ${cfg.mysql.password}

              pteroReplace REDIS_HOST ${cfg.redis.host}
              pteroReplace REDIS_PASSWORD ${redisPassword}
              pteroReplace REDIS_PORT ${builtins.toString cfg.redis.port}
            '';
        in
        {
          ptero-queue-worker = {
            after = [ "redis-server.service" "ptero-panel-setup.service" ];
            path = [ pteroCli ];
            wantedBy = [ "multi-user.target" ];

            script = "ptero-cli queue:work --queue=high,standard,low --sleep=3 --tries=3";
          };

          ptero-pre-panel-setup =
            let
              ptero-env = pkgs.writeText "ptero-panel-env" (import ./panel-env.nix);
            in
            {
              wantedBy = [ "multi-user.target" ];
              after = [ "network-online.target" ];
              path = [ pteroUpdateScript pteroReplace pteroCli ];

              script = ''
                mkdir -p ${cfg.dataDir}
                cd ${cfg.dataDir}

                # Don't install twice
                if test -f ".installed"; then
                    exit 0
                fi

                cp ${ptero-env} .env
                ${scriptConfig}
                pteroUpdate

                touch .installed
              '';

              serviceConfig.Type = "oneshot";
            };

          ptero-panel-setup = {
            wantedBy = [ "multi-user.target" ];
            after = [ "ptero-pre-panel-setup.service" ];
            path = [ pteroReplace ];

            script = scriptConfig;
          };
        };

      networking.firewall.allowedTCPPorts = [ 80 443 ];
      environment.systemPackages = [ pteroCli pteroUpdateScript pteroUpdateFinScript ];

      services.nginx = {
        enable = true;
        virtualHosts."${cfg.url}" = {
          root = "${cfg.dataDir}/public";

          locations."/" = {
            tryFiles = "$uri $uri/ /index.php?$query_string";
          };

          locations."~ /\.ht" = {
            extraConfig = "deny all;";
          };

          locations."~ \.php$" = {
            extraConfig = ''
              fastcgi_split_path_info ^(.+\.php)(/.+)$;
              fastcgi_pass unix:${phpFpm.socket};
              fastcgi_index index.php;
              include ${config.services.nginx.package}/conf/fastcgi.conf;
              fastcgi_param PHP_VALUE "upload_max_filesize = 100M \n post_max_size=100M";
              fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
              fastcgi_param HTTP_PROXY "";
              fastcgi_intercept_errors off;
              fastcgi_buffer_size 16k;
              fastcgi_buffers 4 16k;
              fastcgi_connect_timeout 300;
              fastcgi_send_timeout 300;
              fastcgi_read_timeout 300;
            '';
          };

          extraConfig = ''
            location = /favicon.ico { access_log off; log_not_found off; }
            location = /robots.txt  { access_log off; log_not_found off; }

            access_log off;

            client_max_body_size 100m;
            client_body_timeout 120s;

            sendfile off;

            index index.html index.htm index.php;
            charset utf-8;
          '';
        };
      };
    })
    (mkIf (cfg.enable && config.ronin.useSSL) {
      services.nginx.virtualHosts."${cfg.url}" = {
        enableACME = true;
        forceSSL = true;

        extraConfig = ''
          add_header X-Content-Type-Options nosniff;
          add_header X-XSS-Protection "1; mode=block";
          add_header X-Robots-Tag none;
          add_header Content-Security-Policy "frame-ancestors 'self'";
          add_header X-Frame-Options DENY;
          add_header Referrer-Policy same-origin;
        '';
      };
    })
  ];
}
