{ writeShellScriptBin, curl, jq }:
writeShellScriptBin "hetzner-dns" ''

export API_TOKEN=$(cat $1)
export ZONE_ID=$(cat $2)

function print_toplevel {
    echo "What do you want to do?"
    echo "1) List all DNS Records"
    echo "2) Create new DNS Record"
    echo "3) Delete DNS Record"
    echo "4) Update DNS Record"
    echo "q) Exit"
}

function api_all_records {
    ${curl}/bin/curl -s "https://dns.hetzner.com/api/v1/records?zone_id=$ZONE_ID" \
        -H "Auth-API-Token: $API_TOKEN"
}

function op_all_records {
    records=$(api_all_records)
    
    ${jq}/bin/jq -r '.records[] | [.name, .value, .type] | @csv' <<< "$records" \
        | awk -v FS="," 'BEGIN{printf "%2s. %6s %12s %s\n","No","Type","Name","Ip"; print "----------------------------------------------"}{printf "%2d) %6s %12s %s\n",NR,$3,$1,$2}'
}

function op_create_record {
    read -p "Name? " rc_name
    read -p "Type? " rc_type
    read -p "Ip? " rc_value

    ${curl}/bin/curl -s -X "POST" "https://dns.hetzner.com/api/v1/records" \
        -H 'Content-Type: application/json' \
        -H "Auth-API-Token: $API_TOKEN" \
        -d "{
            \"value\": \"$rc_value\",
            \"ttl\": 86400,
            \"type\": \"$rc_type\",
            \"name\": \"$rc_name\",
            \"zone_id\": \"$ZONE_ID\"
        }" | ${jq}/bin/jq
}

function op_delete_record {
    records=$(api_all_records)

    read -p "No? " rc_no
    rc_no=$((rc_no-1))
    record_id=$(${jq}/bin/jq -r ".records[$rc_no] | .id" <<< "$records")
    record_name=$(${jq}/bin/jq -r ".records[$rc_no] | .name" <<< "$records")
    record_ip=$(${jq}/bin/jq -r ".records[$rc_no] | .value" <<< "$records")

    echo "Are you sure you want to delete \"$record_name($record_ip)\"?"
    read -s -p $'(y/n)?\n' -N 1 confirm
    if [[ $confirm=="y" ]]; then
        ${curl}/bin/curl -s -X "DELETE" "https://dns.hetzner.com/api/v1/records/$record_id" \
            -H "Auth-API-Token: $API_TOKEN" | ${jq}/bin/jq
        echo "\"$record_name($record_ip)\" deleted"
    else
        echo "Cancelled..."
    fi
}

function op_update_record {
    records=$(api_all_records)

    read -p "No? " rc_no
    rc_no=$((rc_no-1))
    record_id=$(${jq}/bin/jq -r ".records[$rc_no] | .id" <<< "$records")
    record_name=$(${jq}/bin/jq -r ".records[$rc_no] | .name" <<< "$records")
    record_ip=$(${jq}/bin/jq -r ".records[$rc_no] | .value" <<< "$records")

    echo "Updating \"$record_name($record_ip)\":"
    
    read -p "Name? " rc_name
    read -p "Type? " rc_type
    read -p "Ip? " rc_value

    ${curl}/bin/curl -s -X "PUT" "https://dns.hetzner.com/api/v1/records/$record_id" \
        -H 'Content-Type: application/json' \
        -H "Auth-API-Token: $API_TOKEN" \
        -d "{
            \"value\": \"$rc_value}\",
            \"ttl\": 86400,
            \"type\": \"$rc_type\",
            \"name\": \"$rc_name\",
            \"zone_id\": \"$ZONE_ID\"
        }" | ${jq}/bin/jq
}

function select_op {
    print_toplevel
    read -s -p $'>\n' -N 1 selection

    case $selection in
        "1")
            op_all_records
            ;;
        "2")
            op_create_record
            ;;
        "3")
            op_delete_record
            ;;
        "4")
            op_update_record
            ;;
        "q")
            exit 0
            ;;
    esac
}

while true; do
    select_op
done

''
