{ stdenv, fetchurl, ... }:
with stdenv;
mkDerivation {
  pname = "adminer";
  version = "4.8.0";

  dontUnpack = true;

  src = fetchurl {
    url = "https://github.com/vrana/adminer/releases/download/v4.8.0/adminer-4.8.0.php";
    sha256 = "1aifwc90x2fw2gqbmk55wx590qlhi1lsj0608206nahg1s72cx72";
  };

  installPhase = ''
    cp $src $out
  '';
}
