{ stdenv, fetchurl }:
with stdenv;
mkDerivation {
  pname = "pterodactyl-panel";
  version = "1.3.2";

  src = fetchurl {
    url = "https://github.com/pterodactyl/panel/releases/download/v1.3.2/panel.tar.gz";
    sha256 = "18bczg4myw4vzp8wxjnbn1xiskcyi9hyjgnm57fg6yxj5vgcn915";
  };

  unpackPhase = ''
    mkdir panel
    cd panel
    tar -xzvf $src
  '';

  installPhase = ''
    chmod -R 755 storage/* bootstrap/cache/
    mkdir -p $out/
    cp -R . $out/
  '';
}
