{ stdenv, fetchurl, curl }:
with stdenv;
mkDerivation {
  pname = "pterodactyl-wings";
  version = "1.3.2";

  dontUnpack = true;

  buildInputs = [ curl ];

  src = fetchurl {
    url = "https://github.com/pterodactyl/wings/releases/download/v1.3.2/wings_linux_amd64";
    sha256 = "0xc1ah9dd0v2rz0dk5j1ba7qi0pwaddwqks4p28r2qgz8misixmd";
  };

  installPhase = ''
    mkdir -p $out/bin
    cp $src $out/bin/wings
    chmod +x $out/bin/wings
  '';
}
