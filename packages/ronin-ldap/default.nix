{ stdenv, lib, writeShellScript, makeWrapper, openldap, miller, gnused, less, unixtools, ... }:
with stdenv;
mkDerivation {
  name = "ronin-ldap";
  src = writeShellScript "ronin-ldap" (builtins.readFile ./ronin-ldap.sh);

  buildInputs = [
    makeWrapper
  ];

  phases = [ "installPhase" ];

  installPhase = ''
    mkdir -p $out $out/bin
    makeWrapper $src $out/bin/ronin-ldap \
        --prefix PATH : "${
            lib.makeBinPath [
                openldap
                gnused
                less
                miller
                unixtools.column
            ]
        }"
  '';
}
