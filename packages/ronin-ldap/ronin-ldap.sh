export BASE_DN="dc=ronin,dc=de"
export USER_DN="ou=user,$BASE_DN"
export GROUP_DN="ou=group,$BASE_DN"

function print_toplevel {
    echo "What do you want to do?"
    echo "1) List all groups"
    echo "2) Create group"
    echo "3) Delete group"
    echo ""
    echo "4) List all users"
    echo "5) Create user"
    echo "6) Change user password"
    echo "7) Delete user"
    echo ""
    echo "8) Add user to group"
    echo "9) Remove user from group"
    echo ""
    echo "i) Init LDAP database"
    echo ""
    echo "q) Exit"
}

function op_group_list {
    ldapsearch -Y EXTERNAL -H ldapi:/// -LLL -Q -b $GROUP_DN objectclass=roninGroup cn member \
        | less
}

function op_user_list {
    ldapsearch -Y EXTERNAL -H ldapi:/// -LLL -Q -b $USER_DN objectclass=roninUser uid mail \
        | sed 's/://g' \
        | sed 's/,/;/g' \
        | mlr --x2c cat \
        | column -t -s, -O 2,3,1 \
        | less
}

function op_group_create {
    echo "New group name?"
    read -p "Name: " grp_name

ldapadd -Y EXTERNAL -Q -H ldapi:/// << EOF
dn: cn=$grp_name,$GROUP_DN
objectclass: roninGroup
cn: $grp_name
EOF

    read -s -p "[ENTER]"
}

function op_group_delete {
    echo "Which group should I delete?"
    read -p "Name: " grp_name

ldapdelete -Y EXTERNAL -Q -H ldapi:/// << EOF
cn=$grp_name,$GROUP_DN
EOF

    echo "Group $grp_name deleted!"
    read -s -p "[ENTER]"
}

function op_user_create {
    echo "Fill in some user information."
    read -p "Name: " usr_name
    read -p "Email: " usr_email
    read -p "Pass: " usr_pass

ldapadd -Y EXTERNAL -Q -H ldapi:/// << EOF
dn: uid=$usr_name,$USER_DN
objectclass: roninUser
uid: $usr_name
mail: $usr_email
userPassword: $usr_pass
EOF

    read -s -p "[ENTER]"
}

function op_user_changepw {
    echo "Which user's password do you want to change?"
    read -p "Name: " usr_name
    read -p "New password: " usr_pass

ldapmodify -Y EXTERNAL -Q -H ldapi:/// << EOF
dn: uid=$usr_name,$USER_DN
changetype: modify
replace: userPassword
userPassword: $usr_pass
EOF

    echo "Password changed!"
    read -s -p "[ENTER]"
}

function op_user_delete {
    echo "Which user should I delete?"
    read -p "Name: " usr_name

ldapdelete -Y EXTERNAL -Q -H ldapi:/// << EOF
uid=$usr_name,$USER_DN
EOF

    echo "User $usr_name deleted!"
    read -s -p "[ENTER]"
}

function op_group_new_user {
    echo "Which user should be added into what group?"
    read -p "Username: " usr_name
    read -p "Group: " grp_name

ldapmodify -Y EXTERNAL -Q -H ldapi:/// << EOF
dn: cn=$grp_name,$GROUP_DN
changetype: modify
add: member
member: uid=$usr_name,$USER_DN
EOF

    echo "User $usr_name added to $grp_name!"
    read -s -p "[ENTER]"
}

function op_group_remove_user {
    echo "Which user should be removed from what group?"
    read -p "Username: " usr_name
    read -p "Group: " grp_name

ldapmodify -Y EXTERNAL -Q -H ldapi:/// << EOF
dn: cn=$grp_name,$GROUP_DN
changetype: modify
delete: member
member: uid=$usr_name,$USER_DN
EOF

    echo "User $usr_name removed from $grp_name!"
    read -s -p "[ENTER]"
}

function op_init_db {
ldapadd -Y EXTERNAL -Q -H ldapi:/// << EOF
dn: $BASE_DN
dc: ronin
o: Ronin
objectclass: dcObject
objectclass: organization

dn: $USER_DN
objectclass: organizationalUnit

dn: $GROUP_DN
objectclass: organizationalUnit
EOF
    echo "OpenLDAP has been initialized!"
    read -s -p "[ENTER]"
}

function select_op {
    print_toplevel
    read -s -p $'>' -N 1 selection
    echo ""

    case $selection in
        "1")
            op_group_list
            ;;
        "2")
            op_group_create
            ;;
        "3")
            op_group_delete
            ;;
        "4")
            op_user_list
            ;;
        "5")
            op_user_create
            ;;
        "6")
            op_user_changepw
            ;;
        "7")
            op_user_delete
            ;;
        "8")
            op_group_new_user
            ;;
        "9")
            op_group_remove_user
            ;;
        "i")
            op_init_db
            ;;
        "q")
            exit 0
            ;;
    esac
}

while true; do
    select_op
done