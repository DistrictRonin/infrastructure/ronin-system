nixpkgs: system:

let
  pkgs = import nixpkgs { inherit system; };
in
{
  nixops-production-shim = pkgs.writeShellScriptBin "nixops-production" "NIXOPS_STATE=secrets/localstate.nixops nixops $@";
  pterodactyl-panel = pkgs.callPackage ./pterodactyl/panel.nix { };
  pterodactyl-wings = pkgs.callPackage ./pterodactyl/wings.nix { };
  adminer = pkgs.callPackage ./adminer.nix { };
  hetzner-dns = pkgs.callPackage ./hetzner-dns.nix { };
  ronin-ldap = pkgs.callPackage ./ronin-ldap/default.nix { };
}

